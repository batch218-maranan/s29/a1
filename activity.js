// I.

db.users.find(
		{
			$or: [
				{firstName: {$regex: 'S',}},
				{firstName: {$regex: 's',}},
				{lastName: {$regex: 'D',}},
				{lastName: {$regex: 'd',}}
			]
		},
		{
			firstName: 1,
			lastName: 1,
			_id: 0
		}
	);

// II.

db.users.find(
		{
			$and: [
				{department: "HR"},
				{age: {$gte: 70}}
				]
        }
    );

// III.

db.users.find(
		{
			$and: [
				{firstName: {$regex: 'e',}},
				{age: {$lte: 30}}
				]
        }
    );